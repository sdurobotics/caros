#include <caros/universal_robots.h>

#include <caros/common.h>
#include <caros/common_robwork.h>

#include <ros/assert.h>

#include <ur_rtde/rtde_control_interface.h>
#include <ur_rtde/rtde_io_interface.h>
#include <ur_rtde/rtde_receive_interface.h>

#include <rw/core/Ptr.hpp>
#include <rw/math/EAA.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Wrench6D.hpp>
#include <rw/trajectory/Path.hpp>

#include <memory>
#include <string>
#include <vector>

namespace caros
{

UniversalRobots::UniversalRobots(const ros::NodeHandle& nodehandle)
    : CarosNodeServiceInterface(nodehandle, RUN_LOOP_FREQUENCY),
      SerialDeviceServiceInterface(nodehandle),
      URActionInterface(nodehandle),
      URServiceInterface(nodehandle),
      nodehandle_(nodehandle)
{
  /* Currently nothing specific should happen */
}

UniversalRobots::~UniversalRobots()
{
  if (rtde_control_ != nullptr)
    rtde_control_->stopJ();
}

bool UniversalRobots::activateHook()
{
  /************************************************************************
   * Parameters
   ************************************************************************/
  if (!nodehandle_.getParam("device_ip", device_ip_))
  {
    CAROS_FATALERROR("The parameter '" << nodehandle_.getNamespace()
                                       << "/device_ip' was not present on the parameter server! "
                                          "This parameter has to be specified for this "
                                          "node to work properly.",
                     URNODE_MISSING_PARAMETER);
    return false;
  }

  if (!nodehandle_.getParam("use_serial_device_interface", use_serial_device_interface_))
  {
    CAROS_FATALERROR("The parameter '" << nodehandle_.getNamespace()
                                       << "/use_serial_device_interface' was not present on the parameter server! "
                                          "This parameter has to be specified for this "
                                          "node to work properly.",
                     URNODE_MISSING_PARAMETER);
    return false;
  }

  /************************************************************************
   * Interfaces
   ************************************************************************/
  try
  {
    rtde_control_ = std::make_shared<ur_rtde::RTDEControlInterface>(device_ip_);
    rtde_io_ = std::make_shared<ur_rtde::RTDEIOInterface>(device_ip_);
    rtde_receive_ = std::make_shared<ur_rtde::RTDEReceiveInterface>(device_ip_);
  }
  catch (rw::common::Exception& exp)
  {
    CAROS_FATALERROR("Could not initialize ur_rtde interface:" << exp.what(), URNODE_COULD_NOT_INIT_UR_RTDE);
    return false;
  }

  if (use_serial_device_interface_)
  {
    if (!URServiceInterface::configureInterface())
    {
      CAROS_FATALERROR("The URService could not be configured correctly.", URNODE_URSERVICE_CONFIGURE_FAIL);
      return false;
    }


    if (!SerialDeviceServiceInterface::configureInterface())
    {
      CAROS_FATALERROR("The SerialDeviceService could not be configured correctly.",
                       URNODE_SERIALDEVICESERVICE_CONFIGURE_FAIL);
      return false;
    }
  }

  return true;
}

bool UniversalRobots::recoverHook(const std::string& error_msg, const int64_t error_code)
{
  bool resolved = false;

  switch (error_code)
  {
    case URNODE_ROBOT_EMERGENCY_STOP:
      if (rtde_control_ != nullptr)
      {
        int32_t safety_mode = rtde_receive_->getSafetyMode();
        if (safety_mode == SAFETY_MODE_NORMAL)
        {
          if (rtde_control_->reuploadScript())
          {
            resolved = true;
            ROS_INFO("Succesfully recovered from URNODE_ROBOT_EMERGENCY_STOP, resuming normal operation");
          }
          else
          {
            resolved = false;
          }
        }
      }

      break;

    case URNODE_ROBOT_PROTECTIVE_STOP:
      if (rtde_control_ != nullptr)
      {
        int32_t safety_mode = rtde_receive_->getSafetyMode();
        if (safety_mode == SAFETY_MODE_NORMAL)
        {
          if (rtde_control_->reuploadScript())
          {
            resolved = true;
            ROS_INFO("Succesfully recovered from URNODE_ROBOT_PROTECTIVE_STOP, resuming normal operation");
          }
          else
          {
            resolved = false;
          }
        }
      }

      break;

    case URNODE_EXECUTION_TIMEOUT:
      if (rtde_control_ != nullptr)
      {
        int32_t safety_mode = rtde_receive_->getSafetyMode();
        if (safety_mode == SAFETY_MODE_NORMAL)
        {
          if (rtde_control_->reuploadScript())
          {
            resolved = true;
            ROS_INFO("Succesfully recovered from URNODE_EXECUTION_TIMEOUT, resuming normal operation");
          }
          else
          {
            resolved = false;
          }
        }
      }

      break;

    case URNODE_UNSUPPORTED_Q_LENGTH:
      /* Simply acknowledge that a wrong Q was provided */
      resolved = true;
      break;
    case URNODE_INTERNAL_ERROR:
      CAROS_FATALERROR("Can not resolve an internal error... ending up in this case/situation is a bug!",
                       URNODE_INTERNAL_ERROR);
      break;
    default:
      CAROS_FATALERROR("The provided error code '"
                           << error_code << "' has no recovery functionality! - this should be considered a bug!",
                       URNODE_INTERNAL_ERROR);
      break;
  }

  return resolved;
}

void UniversalRobots::runLoopHook()
{
  caros_control_msgs::RobotState robot_state;
  robot_state.q = caros::toRos(rw::math::Q(rtde_receive_->getActualQ()));
  robot_state.dq = caros::toRos(rw::math::Q(rtde_receive_->getActualQd()));

  const std::vector< double > std_pose = rtde_receive_->getActualTCPPose();
  rw::math::Vector3D<> pos(std_pose[0], std_pose[1], std_pose[2]);
  rw::math::EAA<> eaa(std_pose[3], std_pose[4], std_pose[5]);
  rw::math::Transform3D<> tcp_pose(pos, eaa.toRotation3D());
  robot_state.tcp_pose = caros::toRos(tcp_pose);

  const std::vector< double > std_w = rtde_receive_->getActualTCPForce();
  rw::math::Wrench6D<> wrench(std_w[0], std_w[1], std_w[2], std_w[3], std_w[4], std_w[5]);
  robot_state.tcp_force = caros::toRos(wrench);

  robot_state.header.frame_id = nodehandle_.getNamespace();
  robot_state.header.stamp = ros::Time::now();
  bool emergency_stopped = false;
  bool protective_stopped = false;
  int32_t safety_mode = rtde_receive_->getSafetyMode();

  if (safety_mode == SAFETY_MODE_ROBOT_EMERGENCY_STOP || safety_mode == SAFETY_MODE_SYSTEM_EMERGENCY_STOP ||
      safety_mode == SAFETY_MODE_SAFEGUARD_STOP)
  {
    emergency_stopped = true;
  }

  if (safety_mode == SAFETY_MODE_PROTECTIVE_STOP)
  {
    protective_stopped = true;
  }

  robot_state.e_stopped = caros::toRos(emergency_stopped);
  robot_state.s_stopped = caros::toRos(protective_stopped);

  if (use_serial_device_interface_)
  {
    URServiceInterface::publishState(robot_state);
  }
  else
  {
    URActionInterface::publishState(robot_state);
  }

  if (protective_stopped)
    CAROS_ERROR("Robot is in protective stop! switching to error state", URNODE_ROBOT_PROTECTIVE_STOP);

  if (emergency_stopped)
    CAROS_ERROR("Robot is in emergency stop! switching to error state", URNODE_ROBOT_EMERGENCY_STOP);
}

void UniversalRobots::errorLoopHook()
{
  // Immediately try to recover from the error
  recoverNode();
}

void UniversalRobots::fatalErrorLoopHook()
{
  /* TODO:
   * Consider what needs to be done when this node is in error - should any of the ur_rtde objects/connections be
   * stopped or just let them continue?
   */
}

/************************************************************************
 * URServiceInterface functions
 ************************************************************************/
bool UniversalRobots::urMoveLin(const rw::math::Transform3D<>& target, double speed, double acceleration)
{
  ROS_DEBUG_STREAM("urMoveLin with target: " << target);

  if (!isInWorkingCondition())
  {
    return false;
  }

  rw::math::Vector3D<> pos = target.P();
  rw::math::EAA<> eaa(target.R());
  std::vector< double > std_pose = {pos[0], pos[1], pos[2], eaa[0], eaa[1], eaa[2]};
  if (rtde_control_->moveL(std_pose, speed, acceleration))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMovePtp(const rw::math::Q& target, double speed, double acceleration)
{
  ROS_DEBUG_STREAM("urMovePtp with target: " << target);

  if (!isInWorkingCondition())
  {
    return false;
  }

  if (!supportedQSize(target))
  {
    return false;
  }

  if (rtde_control_->moveJ(target.toStdVector(), speed, acceleration))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMovePtpPath(const rw::trajectory::QPath& q_path)
{
  ROS_DEBUG("urMovePtpPath with q_path");

  if (!isInWorkingCondition())
  {
    return false;
  }

  std::vector< std::vector< double > > path;
  for (const auto& q : q_path)
      path.push_back(q.toStdVector());
  if (rtde_control_->moveJ(path))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMoveVelQ(const rw::math::Q& q_vel, double acceleration, double time)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->speedJ(q_vel.toStdVector(), acceleration, time))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMoveVelT(const rw::math::VelocityScrew6D<>& t_vel, double acceleration, double time)
{
  if (!isInWorkingCondition())
  {
    return false;
  }
  rw::math::Q xd(6, t_vel[0], t_vel[1], t_vel[2], t_vel[3], t_vel[4], t_vel[5]);
  if (rtde_control_->speedL(xd.toStdVector(), acceleration, time))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMoveVelStop()
{
  ROS_DEBUG_STREAM("Stopping speeding");
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->speedStop())
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urServoStop()
{
  ROS_DEBUG_STREAM("Stopping servos");
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->servoStop())
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urServoQ(const rw::math::Q& target, const float time, const float lookahead_time,
                               const float gain)
{
  ROS_DEBUG_STREAM("ServoQ: " << target);

  if (!isInWorkingCondition() || !supportedQSize(target))
  {
    return false;
  }

  if (rtde_control_->servoJ(target.toStdVector(), 0.0, 0.0, time, lookahead_time, gain))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urForceModeStart(const rw::math::Transform3D<>& refToffset, const rw::math::Q& selection,
                                       const rw::math::Wrench6D<>& wrench_target, int type, const rw::math::Q& limits)
{
  ROS_DEBUG_STREAM("ForceModeStart arguments begin:");
  ROS_DEBUG_STREAM("refToffset: " << refToffset);
  ROS_DEBUG_STREAM("selection: " << selection);
  ROS_DEBUG_STREAM("wrench_target: " << wrench_target);
  ROS_DEBUG_STREAM("type: " << type);
  ROS_DEBUG_STREAM("limits: " << limits);
  ROS_DEBUG_STREAM("ForceModeStart arguments end");

  if (!isInWorkingCondition())
  {
    return false;
  }

  if (selection.size() != 6)
  {
    ROS_WARN_STREAM("The number of elements in selection is '" << selection.size() << "' but should be '" << 6 << "'");
    return false;
  }
  if (limits.size() != 6)
  {
    ROS_WARN_STREAM("The number of elements in limits is '" << limits.size() << "' but should be '" << 6 << "'");
    return false;
  }

  rw::math::Vector3D<> pos = refToffset.P();
  rw::math::EAA<> eaa(refToffset.R());
  std::vector< double > std_task_frame = {pos[0], pos[1], pos[2], eaa[0], eaa[1], eaa[2]};
  std::vector< double > std_selection_vector_double = selection.toStdVector();
  std::vector< int > std_selection_vector(std_selection_vector_double.begin(),
                                          std_selection_vector_double.end());
  const rw::math::Vector3D<> force  = wrench_target.force();
  const rw::math::Vector3D<> torque = wrench_target.torque();
  std::vector< double > std_wrench  = {force[0], force[1], force[2], torque[0], torque[1], torque[2]};

  fm_task_frame_ = std_task_frame;
  fm_selection_ = std_selection_vector;
  fm_type_ = type;
  fm_limits_ = limits.toStdVector();

  if (rtde_control_->forceMode(fm_task_frame_, fm_selection_, std_wrench, fm_type_, fm_limits_))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urForceModeUpdate(const rw::math::Wrench6D<>& wrench_target)
{
  ROS_DEBUG_STREAM("New wrench target for forceModeUpdate: " << wrench_target);

  if (!isInWorkingCondition())
  {
    return false;
  }

  const rw::math::Vector3D<> force  = wrench_target.force();
  const rw::math::Vector3D<> torque = wrench_target.torque();
  std::vector< double > std_wrench  = {force[0], force[1], force[2], torque[0], torque[1], torque[2]};

  if (rtde_control_->forceMode(fm_task_frame_, fm_selection_, std_wrench, fm_type_, fm_limits_))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urForceModeStop()
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->forceModeStop())
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urSetPayload(const double& mass, const rw::math::Vector3D<>& com)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  std::vector<double> cog(3);
  cog.at(0) = com[0];
  cog.at(1) = com[1];
  cog.at(2) = com[2];

  if (rtde_control_->setPayload(mass, cog))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urSetIO(const int& pin, const bool& value)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_io_->setStandardDigitalOut(pin, value))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urMoveStop()
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  rtde_control_->stopJ();

  return true;
}

int UniversalRobots::urGetRobotMode()
{
  return rtde_receive_->getRobotMode();
}

bool UniversalRobots::urSendCustomScriptFile(const std::string& file_path)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->sendCustomScriptFile(file_path))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urSendCustomScriptFunction(const std::string& function_name, const std::string& script)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->sendCustomScriptFunction(function_name, script))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::urZeroFtSensor()
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->zeroFtSensor())
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

/************************************************************************
 * SerialDeviceServiceInterface
 *
 * Notice! only available if use_serial_device_interface is set to true
 *
 ************************************************************************/
bool UniversalRobots::moveLin(const TransformAndSpeedAndAccelerationContainer_t& targets)
{
  ROS_DEBUG_STREAM("moveLin with " << targets.size() << " target(s).");

  if (!isInWorkingCondition())
  {
    return false;
  }

  for (const auto& target : targets)
  {
    rw::math::Vector3D<> pos = std::get<0>(target).P();
    rw::math::EAA<> eaa(std::get<0>(target).R());
    std::vector< double > std_pose = {pos[0], pos[1], pos[2], eaa[0], eaa[1], eaa[2]};
    if (rtde_control_->moveL(std_pose, std::get<1>(target), 0.5))
    {
      return true;
    }
    else
    {
      CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                  URNODE_EXECUTION_TIMEOUT);
      return false;
    }
  }

  return true;
}

bool UniversalRobots::movePtp(const QAndSpeedAndAccelerationContainer_t& targets)
{
  ROS_DEBUG_STREAM("movePtp with " << targets.size() << " target(s).");

  if (!isInWorkingCondition())
  {
    return false;
  }

  for (const auto& target : targets)
  {
    const auto& q = std::get<0>(target);
    if (!supportedQSize(q))
    {
      return false;
    }

    if (rtde_control_->moveJ(q.toStdVector(), std::get<1>(target), std::get<2>(target)))
    {
      return true;
    }
    else
    {
      CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                  URNODE_EXECUTION_TIMEOUT);
      return false;
    }
  }

  return true;
}

bool UniversalRobots::movePtpT(const TransformAndSpeedAndAccelerationContainer_t& targets)
{
  ROS_DEBUG_STREAM("movePtpT with " << targets.size() << " target(s).");

  if (!isInWorkingCondition())
  {
    return false;
  }

  for (const auto& target : targets)
  {
    rw::math::Vector3D<> pos = std::get<0>(target).P();
    rw::math::EAA<> eaa(std::get<0>(target).R());
    std::vector< double > std_pose = {pos[0], pos[1], pos[2], eaa[0], eaa[1], eaa[2]};
    if (rtde_control_->moveJ_IK(std_pose, std::get<1>(target), std::get<2>(target)))
    {
      return true;
    }
    else
    {
      CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                  URNODE_EXECUTION_TIMEOUT);
      return false;
    }
  }

  return true;
}

bool UniversalRobots::moveVelQ(const rw::math::Q& q_vel)
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  if (rtde_control_->speedJ(q_vel.toStdVector(), 0.5, 0.5))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::moveVelT(const rw::math::VelocityScrew6D<>& t_vel)
{
  if (!isInWorkingCondition())
  {
    return false;
  }
  rw::math::Q xd(6, t_vel[0], t_vel[1], t_vel[2], t_vel[3], t_vel[4], t_vel[5]);
  if (rtde_control_->speedL(xd.toStdVector(), 0.5, 0.5))
  {
    return true;
  }
  else
  {
    CAROS_ERROR("Execution timeout received! The robot might be in protective or emergency stop.",
                URNODE_EXECUTION_TIMEOUT);
    return false;
  }
}

bool UniversalRobots::moveStop()
{
  if (!isInWorkingCondition())
  {
    return false;
  }

  rtde_control_->stopJ();

  return true;
}

/************************************************************************
 * Utility functions
 ************************************************************************/
bool UniversalRobots::isInWorkingCondition()
{
  if (!isInRunning())
  {
    ROS_WARN_STREAM("Not in running state!");
    return false;
  }

  return true;
}

bool UniversalRobots::supportedQSize(const rw::math::Q& q)
{
  if (q.size() != SUPPORTED_Q_LENGTH_FOR_UR)
  {
    CAROS_ERROR("The length of Q is " << q.size() << " but should be " << SUPPORTED_Q_LENGTH_FOR_UR,
                URNODE_UNSUPPORTED_Q_LENGTH);
    return false;
  }

  return true;
}

}  // namespace caros
