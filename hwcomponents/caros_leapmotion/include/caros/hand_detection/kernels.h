#ifndef CAROS_HAND_DETECTION_KERNELS_H
#define CAROS_HAND_DETECTION_KERNELS_H

#define GAUSSIAN_9                                                                          \
  {                                                                                         \
    0.028532, 0.067234, 0.124009, 0.179044, 0.20236, 0.179044, 0.124009, 0.067234, 0.028532 \
  }

#endif  // CAROS_HAND_DETECTION_KERNELS_H
